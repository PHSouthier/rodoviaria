CREATE DATABASE rodoviaria;
USE rodoviaria;

CREATE TABLE passageiro (
    idPassageiro int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nome varchar(50),
    genero varchar(50),
    RG int,
    CPF varchar(14),
    endereco varchar(150),
    email varchar(70),
    telefone int
);

