package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import model.bean.Passageiro;
import model.dao.PassageiroDAO;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class JFCadastrarPassageiro extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtEndereco;
	private JTextField txtEmail;
	private JTextField txtCpf;
	private JTextField txtRg;
	private JTextField txtTelefone;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFCadastrarPassageiro frame = new JFCadastrarPassageiro();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFCadastrarPassageiro() {
		setTitle("SisRodoviaria - Cadastrar Passageiro");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 620, 405);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Cadastrar Passageiro");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(10, 11, 131, 24);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Nome");
		lblNewLabel_1.setBounds(10, 46, 46, 14);
		contentPane.add(lblNewLabel_1);
		
		txtNome = new JTextField();
		txtNome.setBounds(10, 65, 571, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Endere\u00E7o");
		lblNewLabel_2.setBounds(10, 96, 87, 14);
		contentPane.add(lblNewLabel_2);
		
		txtEndereco = new JTextField();
		txtEndereco.setBounds(11, 121, 570, 20);
		contentPane.add(txtEndereco);
		txtEndereco.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("E-mail");
		lblNewLabel_3.setBounds(10, 152, 46, 14);
		contentPane.add(lblNewLabel_3);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(10, 177, 571, 20);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("CPF");
		lblNewLabel_4.setBounds(10, 208, 46, 14);
		contentPane.add(lblNewLabel_4);
		
		txtCpf = new JTextField();
		txtCpf.setBounds(11, 233, 263, 20);
		contentPane.add(txtCpf);
		txtCpf.setColumns(10);
		
		JLabel lblNewLabel_5 = new JLabel("RG");
		lblNewLabel_5.setBounds(313, 208, 46, 14);
		contentPane.add(lblNewLabel_5);
		
		txtRg = new JTextField();
		txtRg.setBounds(312, 233, 258, 20);
		contentPane.add(txtRg);
		txtRg.setColumns(10);
		
		JLabel lblNewLabel_6 = new JLabel("G\u00EAnero");
		lblNewLabel_6.setBounds(313, 264, 85, 14);
		contentPane.add(lblNewLabel_6);
		
		JRadioButton rdbM = new JRadioButton("Masculino");
		rdbM.setBounds(311, 285, 87, 23);
		contentPane.add(rdbM);
		
		JRadioButton rdbF = new JRadioButton("Feminino");
		rdbF.setBounds(399, 285, 109, 23);
		contentPane.add(rdbF);
		
		ButtonGroup genero = new ButtonGroup();
		genero.add(rdbM);
		genero.add(rdbF);
		
		JLabel lblNewLabel_7 = new JLabel("Telefone");
		lblNewLabel_7.setBounds(10, 264, 100, 14);
		contentPane.add(lblNewLabel_7);
		
		txtTelefone = new JTextField();
		txtTelefone.setBounds(11, 286, 263, 20);
		contentPane.add(txtTelefone);
		txtTelefone.setColumns(10);
		
		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Passageiro p = new Passageiro();
				PassageiroDAO dao = new PassageiroDAO();
				
				p.setNome(txtNome.getText());
				p.setEndereco(txtEndereco.getText());
				p.setEmail(txtEmail.getText());
				p.setCpf(txtCpf.getText());
				p.setRg(Integer.parseInt(txtRg.getText()));
				if(rdbM.isSelected()) {
					p.setGenero("Masculino");
				}else if(rdbF.isSelected()) {
					p.setGenero("Feminino");
				}
				p.setTelefone(Integer.parseInt(txtTelefone.getText()));
				
				dao.create(p);
				dispose();
			}
		});
		btnCadastrar.setBounds(10, 332, 198, 23);
		contentPane.add(btnCadastrar);
		
		JButton btnLimpar = new JButton("Limpar");
		btnLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtNome.setText(null);
				txtEndereco.setText(null);
				txtEmail.setText(null);
				txtCpf.setText(null);
				txtRg.setText(null);
				txtTelefone.setText(null);
				genero.clearSelection();
			}
		});
		btnLimpar.setBounds(218, 332, 180, 23);
		contentPane.add(btnLimpar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setBounds(408, 332, 173, 23);
		contentPane.add(btnCancelar);
	}
}
