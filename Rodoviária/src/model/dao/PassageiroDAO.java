package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import connection.ConnectionFactory;
import model.bean.Passageiro;

public class PassageiroDAO {

	public void create(Passageiro p) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		
		try {
			stmt = con.prepareStatement("INSERT INTO passageiro (nome, endereco, email, RG, CPF, genero, telefone) VALUES (?,?,?,?,?,?,?)");
			stmt.setString(1, p.getNome());
			stmt.setString(2, p.getEndereco());	
			stmt.setString(3, p.getEmail());
			stmt.setInt(4, p.getRg());
			stmt.setString(5, p.getCpf());
			stmt.setString(6, p.getGenero());
			stmt.setInt(7, p.getTelefone());
			
			stmt.executeUpdate();
			JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
		}catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao salvar" + e);
		} finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
	}
	
	public List<Passageiro> read(){
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Passageiro> passageiros = new ArrayList<>();
		
		try {
			stmt = con.prepareStatement("SELECT * FROM passageiro");
			rs = stmt.executeQuery();
			while(rs.next()) {
				Passageiro p = new Passageiro();
				p.setIdPassageiro(rs.getInt("idPassageiro"));
				p.setNome(rs.getString("nome"));
				p.setGenero(rs.getString("genero"));
				p.setRg(rs.getInt("RG"));
				p.setCpf(rs.getString("CPF"));
				p.setEndereco(rs.getString("endereco"));
				p.setEmail(rs.getString("email"));
				p.setTelefone(rs.getInt("telefone"));
				passageiros.add(p);
			}
			
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao exibir as informa��es do BD" + e);
			e.printStackTrace();
		}finally {
			ConnectionFactory.closeConnection(con, stmt, rs);
		}
		return passageiros;
	}
	
	public void delete(Passageiro p) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		
		try {
			stmt = con.prepareStatement("DELETE FROM passageiro WHERE idPassageiro=?");
			stmt.setInt(1, p.getIdPassageiro());
			stmt.executeUpdate();
			
			JOptionPane.showMessageDialog(null, "Passageiro exclu�do com sucesso!");
		}catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao excluir: " + e);
		} finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
	}
	
	public Passageiro read(int id) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Passageiro p = new Passageiro();
		
		try {
			stmt = con.prepareStatement("SELECT * FROM passageiro WHERE idPassageiro=? LIMIT 1;");
			stmt.setInt(1, id);
			rs = stmt.executeQuery();
			if(rs != null && rs.next()) {
				p.setIdPassageiro(rs.getInt("idPassageiro"));
				p.setNome(rs.getString("nome"));
				p.setGenero(rs.getString("genero"));
				p.setRg(rs.getInt("RG"));
				p.setCpf(rs.getString("CPF"));
				p.setEndereco(rs.getString("endereco"));
				p.setEmail(rs.getString("email"));
				p.setTelefone(rs.getInt("telefone"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			ConnectionFactory.closeConnection(con, stmt, rs);
		}
		return p;
	}
	
	public void update(Passageiro p) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		
		try {
			stmt = con.prepareStatement("UPDATE passageiro SET nome=?, genero=?, RG=?, CPF=?, endereco=?, email=?,"
					+ " telefone=? WHERE idPassageiro=?;");
			stmt.setString(1, p.getNome());
			stmt.setString(2, p.getGenero());
			stmt.setInt(3, p.getRg());
			stmt.setString(4, p.getCpf());
			stmt.setString(5, p.getEndereco());
			stmt.setString(6, p.getEmail());
			stmt.setInt(7, p.getTelefone());
			stmt.setInt(8, p.getIdPassageiro());
			stmt.executeUpdate();
			
			JOptionPane.showMessageDialog(null, "Alterado com sucesso!");
		}catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao alterar" + e);
		} finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
	}
	
}
